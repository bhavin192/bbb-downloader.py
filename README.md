# bbb-downloader.py

bbb-downloader.py is a Python script to download BigBlueButton
recording files. These files can later be viewed or hosted with the
help of the
[bbb-playback](https://github.com/bigbluebutton/bbb-playback) project.

## How to use?
The Python script bbb-downloader.py doesn't require any other
dependencies than [Python 3](https://www.python.org/) and
[curl](https://curl.se/).

- Download bbb-downloader.py:
  ```console
  $ curl -O https://gitlab.com/bhavin192/bbb-downloader.py/-/raw/master/bbb-downloader.py
  $ chmod +x bbb-downloader.py
  ```
- Command line options:
  ```console
  $ ./bbb-downloader.py --help
  usage: bbb-downloader.py [-h] -u URL [-d DIR]

  Download BigBlueButton recording files.

  options:
    -h, --help         show this help message and exit
    -u URL, --url URL  meeting recording URL
    -d DIR, --dir DIR  destination directory for the files (default: current directory)
  ```
- Download the recording files to the current directory:
  ```console
  $ ./bbb-downloader.py \
      --url https://test.examplebbb.org/playback/presentation/2.0/playback.html?meetingId=123476b7645df093745-1656145644091
  …
  Use the curl config to download the recording files:
  curl --config 123476b7645df093745-1656145644091.conf

  $ curl --config 123476b7645df093745-1656145644091.conf
  ```
- Download the recording files to a specific directory:
  ```console
  $ ./bbb-downloader.py \
      --url https://test.examplebbb.org/playback/presentation/2.0/playback.html?meetingId=123476b7645df093745-1656145644091 \
      --dir recorddir
  …
  Use the curl config to download the recording files:
  curl --config recorddir-123476b7645df093745-1656145644091.conf

  $ curl --config recorddir-123476b7645df093745-1656145644091.conf
  ```

### How does it work?
bbb-downloader.py relies on curl's configuration file option
`--config` for downloading the files. It generates a curl
configuration file for all the required recording files.

## Credits & Licensing
This project has taken references from the following projects:
- [https://github.com/maikmerten/bbb-downloader/](https://github.com/maikmerten/bbb-downloader/blob/master/src/main/java/de/maikmerten/bbbdownload/downloader/Downloader.java)
- <https://github.com/bigbluebutton/bbb-playback/blob/develop/src/config.js>

bbb-downloader.py is licensed under GNU General Public License
v3.0. See [LICENSE](./LICENSE) for the full license text.

