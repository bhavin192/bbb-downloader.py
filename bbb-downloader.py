#!/usr/bin/env python3

# bbb-downloader.py: Download BigBlueButton recording files.
# Copyright (C) 2022  Bhavin Gandhi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os.path
import sys
import xml.etree.ElementTree as ET
from urllib.parse import urlparse, parse_qsl, urljoin
from urllib.request import urlopen

files = [
    "captions.json",
    "cursor.xml",
    "deskshare.xml",
    "shapes.svg",
    "metadata.xml",
    "panzooms.xml",
    "presentation_text.json",
    "external_videos.json",
    "polls.json",
    "notes.html",
    "slides_new.xml",
    "audio/audio.ogg",
    "audio/audio.webm",
    "video/webcams.webm",
    "video/webcams.mp4",
    "presentation/deskshare.png",
    "deskshare/deskshare.webm",
    "deskshare/deskshare.mp4",
]


def parse_recording_url(recording_url):
    # TODO: if BBB doesn't allow to be run on a path of a domain, this
    # can be simplified using urllib.parse.urljoin(recording_url, "/")
    base_url = recording_url.split("playback/presentation")[0]
    # https://stackoverflow.com/a/21584580
    query_params = dict(parse_qsl(urlparse(recording_url).query))
    meeting_id_key = "meetingId"
    meeting_id = query_params.get(meeting_id_key)
    if not meeting_id:
        raise ValueError(
            f"{meeting_id_key!r} query parameter is not present in the URL"
        )
    return base_url, meeting_id


def get_slide_urls(presentation_url):
    # shapes.svg has links to slide images, alt text, etc.
    print("Fetching the shapes.svg from the server")
    shapes_svg_xml = None
    with urlopen(urljoin(presentation_url, "shapes.svg")) as resp:
        shapes_svg_xml = ET.fromstring(resp.read().decode("utf-8"))

    slide_urls = set()
    href_key = "{http://www.w3.org/1999/xlink}href"
    for slide in shapes_svg_xml:
        image = slide.attrib[href_key]
        slide_urls.add(image)
        slide_urls.add(image.replace("slide-", "thumbnails/thumb-"))
        if "text" in slide.attrib:
            slide_urls.add(slide.attrib["text"])
    return list(slide_urls)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Download BigBlueButton recording files."
    )
    parser.add_argument("-u", "--url", required=True, help="meeting recording URL")
    parser.add_argument(
        "-d",
        "--dir",
        default=".",
        help="destination directory for the files (default: current directory)",
    )
    args = parser.parse_args()

    base_url, meeting_id = parse_recording_url(args.url)
    print(f"BigBlueButton base URL: {base_url}")
    presentation_url = urljoin(base_url, f"presentation/{meeting_id}/")
    slide_urls = get_slide_urls(presentation_url)

    files.extend(slide_urls)
    curl_conf_file = meeting_id + ".conf"
    if args.dir != ".":
        curl_conf_file = args.dir + "-" + curl_conf_file
    # TODO: Should have concurrent download?
    print(f"Generating curl configuration file '{curl_conf_file}'")
    with open(curl_conf_file, "w") as curl_conf:
        for file in files:
            file_path = os.path.join(args.dir, file)
            file_url = urljoin(presentation_url, file)
            curl_conf.write(f'output = "{file_path}"\n')
            curl_conf.write(f'url = "{file_url}"\n')
            curl_conf.write(
                'write-out = "%{onerror}%{urlnum}: %{url}: %{errormsg}\\n"\n'
            )
            curl_conf.write("create-dirs\nfail\n\n")
    print("Use the curl config to download the recording files:")
    print(f"curl --config {curl_conf_file}")
